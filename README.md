# Fallout: New Vegas

This is the [Portmod](https://gitlab.com/portmod/portmod) package repository for Fallout 4.

This requires the use of a custom version of bsatool, [bsatool2](https://gitlab.com/bmwinger/bsatool2).

Eventually you will be able to find details on how to get started on the Portmod [Guide](https://gitlab.com/portmod/portmod/-/wikis/home#guide).

For now, this repository also assumes that you have all the DLC, excluding the High Resolution Texture Pack. 

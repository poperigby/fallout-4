# Copyright 2022 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from pybuild import File, InstallDir

from common.fallout_4 import Fallout4
from common.nexus import NexusMod


class Package(NexusMod, Fallout4):
    NAME = "Tactical Reload"
    DESC = "Adds a second reload for when the magazine is not empty"
    HOMEPAGE = "https://www.nexusmods.com/fallout4/mods/49444"
    NEXUS_URL = HOMEPAGE
    LICENSE = "all-rights-reserved"
    KEYWORDS = "~fallout-4"
    RDEPEND = "vanilla-weapons? ( assets-animations/combat-shotgun-rifle-reanimation )"
    TIER = "z"
    IUSE = "manual-reload dlc-weapons vanilla-weapons"
    SRC_URI = """
        !manual-reload? ( Tactical_Reload_Framework-49444-1-1-1623428864.zip )
        manual-reload? ( Tactical_Reload_Framework_Manual_Reload-49444-1-1-1611737714.zip )
        vanilla-weapons? ( Tactical_Reload_Vanilla_Weapons-49444-1-1-1611737782.zip )
        dlc-weapons? ( Tactical_Reload_DLC_Weapons-49444-1-1-1611737749.zip )
    """
    INSTALL_DIRS = [
        InstallDir(
            ".",
            S="Tactical_Reload_Framework-49444-1-1-1623428864",
            PLUGINS=[File("TacticalReload.esm")],
            REQUIRED_USE="!manual-reload",
        ),
        InstallDir(
            ".",
            S="Tactical_Reload_Framework_Manual_Reload-49444-1-1-1611737714",
            PLUGINS=[File("TacticalReload.esm")],
            REQUIRED_USE="manual-reload",
        ),
        InstallDir(
            ".",
            S="Tactical_Reload_Vanilla_Weapons-49444-1-1-1611737782",
            PLUGINS=[File("TacticalReload.esm")],
            REQUIRED_USE="vanilla-weapons",
        ),
        InstallDir(
            ".",
            S="Tactical_Reload_Vanilla_Weapons-49444-1-1-1611737782",
            PLUGINS=[File("TacticalReloadVanilla.esp")],
            REQUIRED_USE="vanilla-weapons",
        ),
        InstallDir(
            ".",
            S="Tactical_Reload_DLC_Weapons-49444-1-1-1611737749",
            PLUGINS=[File("TacticalReloadDLC.esp")],
            REQUIRED_USE="dlc-weapons",
        ),
    ]

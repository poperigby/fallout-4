# Copyright 2022 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from pybuild import File, InstallDir

from common.fallout_4 import Fallout4
from common.nexus import NexusMod


class Package(NexusMod, Fallout4):
    NAME = "Service Rifle"
    DESC = "Adds the Service Rifle from Fallout New Vegas, based on the legendary M16 platform"
    HOMEPAGE = "https://www.nexusmods.com/fallout4/mods/32361"
    NEXUS_URL = """
        https://www.nexusmods.com/fallout4/mods/32361
        new-sounds? ( https://www.nexusmods.com/fallout4/mods/32975 )
    """
    LICENSE = "all-rights-reserved"
    KEYWORDS = "~fallout-4"
    IUSE = "new-sounds"
    SRC_URI = """
        Service_Rifle-32361-0-96.zip
        new-sounds? ( Alternate_AR15_Sounds_Redux_-_Main-32975-1-0.rar )
    """
    INSTALL_DIRS = [
        InstallDir(
            ".",
            S="Service_Rifle-32361-0-96",
            PLUGINS=[File("F4NVServiceRifleRedux.esp")],
        ),
        InstallDir(
            ".",
            S="Alternate_AR15_Sounds_Redux_-_Main-32975-1-0",
            REQUIRED_USE="new-sounds",
        ),
    ]
